---
title: Instant Messengers
draft: false
layout: single
---
As I refrain from using the overly loved WhatsApp, people often question me on how they can reach me. A simple call always works for a quick chat. But if you are like me who prefers texting, following are the platforms where you can find me.

## Element (Matrix)</a></h2>
[`@dhanesh:maitri.club`]("https://matrix.to/#/@dhanesh:maitri.club")

Element has all the features that you'd expect from an instant messenger including end-to-end encryption, calls, file sharing and support for multiple devices.

If you decide to give Element a try, I'd suggest not to sign up on the [matrix.org]("https://matrix.org") server and choose a different one instead. Here's a [possible list of public servers]("https://servers.joinmatrix.org/") you can choose from. Or, just connect with me on an alternative platform and I might be able to create an account for you on my private instance.

## Signal
`@dhanesh.18`

Signal has a good reputation in the cryptography community and is one of the simplest apps to use. It can be considered as a drop-in replacement for WhatsApp.

## XMPP
`dhanesh95@disroot.org`

XMPP is an open, decentralized messaging protocol that prioritizes privacy and flexibility. It supports text, file transfers, video calls, and group chats. A quick start app can be [Quicksy](https://quicksy.im/).

> If you want to learn more about XMPP, [this blog post](https://contrapunctus.codeberg.page/the-quick-and-easy-guide-to-xmpp.html) is worth your time.