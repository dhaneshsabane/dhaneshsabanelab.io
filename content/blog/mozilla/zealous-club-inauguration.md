---
title: Zeal-ous club inauguration
date: 2015-08-18
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
categories: [Community]
author: Dhanesh Sabane
description: Zeal Firefox Club Inauguration
---

The first month of being a FSA had a huge learning curve. There were abundant things that I had no knowledge of but I was always excited to learn more. Amidst the learning phase, I applied for being a Campaigning Controller for 'Zeal Firefox Club' and even got selected for the same. The club had a team of coordinators and it was time to mark the inception of the club. This brings us to the inauguration ceremony of Zeal Firefox Club.

## Event details:

- Name : Zeal Firefox Club Inauguration
- Agenda :
    - Club inauguration and introduction to Mozilla
- Day and Date: Friday, 10th July 2015
- Venue : Seminar Hall, ZES's Zeal College of Engineering and Research, Narhe, Pune
- Guests:
    - Siddhartha Rao
    - Shubham Bhalerao
    - Shagufta Methwani

-----------------------

The inauguration was held on 10th July 2015. Prior to the event, there was a whole lot of preparation needed. I gave my creativity a second call (after the #FoxYeah campaign) and began with the designing of our club logo. It took me a few days and I fixated on a logo from a bunch of prototypes. Have a look:

![ZFC-Logo](/images/mozilla/zfc-logo.png)

While the team was busy with the preparation for the inauguration, I was busy devising ways to achieve maximum participation from the students. Visiting every class room and inviting them personally was my first priority. Meanwhile, the anchor for our event needed a script to work on and I helped him through it. On the day of the inauguration function, all the members of the team gathered in the college early in the morning and we worked towards setting up the Seminar Hall and the ZFC Lab and finishing off some rough edges.

The inauguration function was a huge success and our guests for the event (Siddhartha Rao, Shagufta Methwani, Shubham Bhalerao) appreciated us for our full fledged efforts. They were overwhelmed by the response and they had never expected something like this would turn up.


The team with the guests:

![ZFC-Team-and-speakers-with-attendees](/images/mozilla/zfc-inauguration.jpg)

<br />
![ZFC-Team-with-speakers](/images/mozilla/zfc-team-with-speakers.jpg)


People often ask me the reason behind contributing to an open source community. I always answer with a huge grin - 'Joy and satisfaction'.

Love Mozilla! <3
