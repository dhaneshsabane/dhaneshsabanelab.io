---
title: MozCafe Dec'15
date: 2015-12-30
tags:
- Firefox
- FSA
- Mozilla
- Open Source
categories: [Community]
author: Dhanesh Sabane
description: MozCafe meeting report.
---

The final MozCafe meeting of the year of the Mozilla Pune Community was held on 19th Dec 2015, Saturday. This was my first ever community meet since being a Mozillian in June. The community members gathered over coffee at C-DAC, Aundh.

The simple agenda for the meet was:

1. To make the newbies familiar with the community activities.

2. Introduction to various Task Forces

3. MozCafe reforms

4. Several contribution gateways

5. Mozilla goals for 2016

6. and Privacy month (January 2016)

The meet began with the task force owners explaining about their respective task forces. Everyone tried their best to rope in some contributors for their respective task force.

![Task Force List](/images/mozilla/mozcafe_dec15_task_forces.webp)

Later, the reformed format of the MozCafe meetings was introduced to the attendees.

![Introduction to the reformed format](/images/mozilla/mozcafe_dec15_attendees_1.webp)

January is declared as 'Privacy Month'. Siddhartha Rao, Ankit Gadgil, Diwanshi and other Privacy TFT members had updates about it. They mentioned the different plans which will be undertaken for the month. One privacy tip for each day of the month will be the activity of the Privacy TFT for January.

![Privacy Month Briefing](/images/mozilla/mozcafe_dec15_attendees_2.webp)

Ankit Gadgil then came up to speak about the Mozilla goals for 2016. He informed the attendees about the contribution areas to focus on for the upcoming year. He also talked about other similar news and Mozilla reforms.

![Recent Mozilla Updates](/images/mozilla/mozcafe_dec15_attendees_3.webp)

Later, the attendees discussed about several other topics. These topics included suggestions along with future activity discussions.

![Activity Discussion](/images/mozilla/mozcafe_dec15_activity_discussion.webp)

The last MozCafe of the year ended with great enthusiasm, important decisions for the upcoming year and a new zeal for contributions.

![Group Photo](/images/mozilla/mozcafe_dec15_group_photo.webp)
