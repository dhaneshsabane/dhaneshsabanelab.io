---
title: "The day when it all began...."
date: 2015-08-14
categories: [Community]
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
author: Dhanesh Sabane
description: Beginning of the Mozilla journey
---

It was just another tedious afternoon at the college. I was in a practical session and every person in the lab was hoping for a distraction. I was surfing through some articles on the Web when Nishigandha Yadav (our current Club Lead) stepped in. She made an announcement about a Firefox Club. She gave an explanation on the club aims. Never had I come across any such programs from Mozilla and it took me by surprise. It got me thinking and I realized that this may exactly be the breakthrough for me. Being an open source enthusiast, I wanted to contribute to the open source community and a golden opportunity had come knocking on my door.

I put on my thinking cap and diverted to searching for ways to contribute to Mozilla. There were endless possible opportunities! Seldom has any organization gone this far to simplify the tasks for it's contributors. I also read about the rewards provided for even the tiniest of contributions.

This was a golden opportunity! I had to seize this chance and gear up to bring my intentions into actions.

A day of finally finding myself eager to be an useful force in the open source community!

Let the contributions begin!

Thank you <a href="https://mozillians.org/en-US/u/Nishigandha/">Nishigandha</a>! :D
