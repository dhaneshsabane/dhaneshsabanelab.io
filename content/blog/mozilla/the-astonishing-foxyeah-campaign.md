---
title: The astonishing FoxYeah campaign!
date: 2015-08-16
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
categories: [Community]
author: Dhanesh Sabane
description: The FoxYeah! Campaign
---

Here I was in my first month of being a FSA (Firefox Student Ambassador) when the FoxYeah campaign came along. The campaign, in short, was all about the contributors boasting their love for Mozilla and their creative skills. We had to create awesome photos or videos to convey why Mozilla is what everyone should choose. All the contributors were on a rampage! There were instant #FoxYeah uploads and many became viral!

So, after keenly having a look at all the awesome posts, I decided to get on board too. I, honestly, am not an ingenious person. I had to brainstorm my way towards generating some clever pictures and I came up with a few simple one's.

Here are a couple of them:

![Firefox-Secure-1](/images/mozilla/foxyeah.jpg)

![Firefox-Secure-2](/images/mozilla/foxyeah-1.png)

<br />
Now these may not be very creative but I did try and I'm proud of myself! :P

Nevertheless, it was a great experience to be an active part of the campaign and I'm sure Team Mozilla was overwhelmed by the response the campaign received through the period. Experiences such as these are more to come and there will always be something to write about when it comes to being an active FSA.

Love Mozilla! <3
