---
title: MozCafé Feb’16
date: 2016-03-25
tags:
- Firefox
- FSA
- Mozilla
- Open Source
author: Dhanesh Sabane
description: MozCafé meetup report
categories: [Community]
---

### Agenda:
* Updates from Leadership Summit, Singapore
* Updates of Privacy month
* Planning of International Woman's Day
* Updates of Makerfest 2016
* Super March planning
* Knowledge sharing session

### Day and Date:

Saturday, 6th Feb 2016

### Venue:

CDAC, Pune

The MozCafé began with updates about Leardership Summit, Singapore from Sayak Sarkar.

![Leadership Summit Update](/images/mozilla/mozcafe_feb16_sayak.webp)


In addition to that, Shagufta also shared her updates on connected devices from the Summit.

![Connected Devices Update](/images/mozilla/mozcafe_feb16_shagufta.webp)


After the Leadership updates, it was time to assess the social impact that the Privacy Month had. The month of January had just ended a week ago and Privacy Month campaign initiated by Mozilla India also came to an end. Siddhartha Rao enlightened the Mozillians with the updates.

![PrivacyMonth Impact Report](/images/mozilla/mozcafe_feb16_privacy_month_impact_report.webp)


The figures indicated that the participation of women in the campaign was unfortunately extremely low. With regards to that, Priyanka Nag came up with the idea of celebrating International Women's Day wherein the community would be encouraging women to participate more. She also mentioned about Super March and how we could use it as a platform to conduct more women centric events throughout the month.

![Attendees](/images/mozilla/mozcafe_feb16_attendees-1.webp)

![Attendees](/images/mozilla/mozcafe_feb16_attendees_2.webp)


After the Super March planning, Aman Sehgal gave the updates on MakerFest 2016, Ahmedabad. He even gave us a sneak-peek at the MakerFest video which was yet to be released.

[Watch the official release of the video](https://www.youtube.com/watch?v=NhBDD26pdDM).


The final part of the meet was a knowledge sharing session. We had decided to undertake such sessions to enhance the community's knowledge about Mozilla and it's products. This was the first session and we had decided to talk on plugins.

Aman Sehgal initiated the session with [uMatrix](https://addons.mozilla.org/en-US/firefox/addon/umatrix/). Later, Rishita Bansal talked on [Lightbeam](https://addons.mozilla.org/en-US/firefox/addon/lightbeam/).  And finally, I gave a brief introduction about [Zenmate](https://addons.mozilla.org/en-US/firefox/addon/zenmate-security-privacy-vpn/).
