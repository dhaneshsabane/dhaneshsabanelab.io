---
title: Firefox for Windows 10 - Campaign Challenge
date: 2015-09-17
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
categories: [Community]
author: Dhanesh Sabane
description: Campaign to promote Firefox Browser on Windows 10
---

## Event details:

**Name** : [Firefox for Windows 10 : Campaign Challenge](https://www.facebook.com/events/435816503277683/)

**Agenda** :
1. Promoting Firefox for Windows 10.
2. To educate our users about how they can maintain their choices and preferences before and after their upgrade experience.

**Day and Date** : Tuesday, 15th September 2015

**Venue** : Computer Department, ZES's Zeal College of Engineering and Research, Narhe, Pune

**Speaker** : [Sumedh Bhalerao](https://mozillians.org/en-US/u/sumedhgpb/)


On an auspicious day which is celebrated as 'Engineers Day', the club decided to hold this event which could help all engineers in the college to review their choices, preferences and privacy on the Web.

The event was split into two parts where the first part was setting up a table at the campus and promote the use of Firefox.

![Firefox for Windows 10 Campaign Setup](/images/mozilla/win10_campaign_1.jpg)

![Firefox for Windows 10 Campaigning](/images/mozilla/win10_campaign_2.jpg)

We did some great progress by speaking to all the approaching students about Firefox and the event motto.

The next part was an informal discussion session. Sumedh Bhalerao was our speaker for the day and he shared some wonderful insights with the attendees.

![Firefox for Windows 10: Sumedh's Talk](/images/mozilla/win10_campaign_talk.jpg)

After having long talks on Mozilla and Firefox for almost an hour and a half, the event concluded with a thank you note. The August Office Hours was a huge source of inspiration for this event.

Hala Mozilla! <3
