---
title: It's official! I'm now a trainee!
date: 2015-09-16
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
categories: [Community]
author: Dhanesh Sabane
description: Step up to being a "trainee" in the Firefox Student Ambassadors' Programme
---

Achieving higher levels of recognition at Mozilla is always a thrilling experience ! Another step towards success !

![FSA Trainee](/images/mozilla/trainee.png)

![Recognition List](/images/mozilla/recognition_list.jpg)

Love Mozilla! <3

Blaze your own path!
