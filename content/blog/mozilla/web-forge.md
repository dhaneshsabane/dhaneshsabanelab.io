---
title: Web Forge
date: 2017-03-13
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
author: Dhanesh Sabane
description: Zeal Firefox Club Event - Web Forge
categories: [Community]
---

## Event details:

* Name : Web Forge
* Agenda :
    1. Working with version control systems – git
    2. Introduction to WebVR (A-Frame)
    3. Game development in Python
* Day and Date : Saturday, 25th February 2017
* Venue : IBM Lab (301), Computer Department, ZES’s Zeal College of Engineering and Research, Narhe, Pune
* Speakers :
    1. [Dhanesh B. Sabane](https://mozillians.org/en-US/u/dhanesh95/)
    2. [Shubham P. Yadav](https://mozillians.org/en-US/u/yadavs4shubham/)

After registering myself as the Club Captain of [Mozilla Club Zeal](https://mozclubzeal.github.io/), it was time to get to work and make the club the most active club on campus. The month of January was particularly busy because me and a few other veterans of the club had to pick out the best students from the campus to be the Executive members of the club. We got off to a great start in the year 2017. After carefully picking the right candidates for Technology Lead, Protect Lead and Teach Lead we started brainstorming about what our first event as Mozilla Campus Club will be. As the club had to conduct activities under all the three domains, we thought why not bring them together and conduct a **grand** event? This little thought paved the way to our club’s first ever event of 2017 as a Mozilla Campus Club.


## INTRODUCTIONS

All the pre-event preparations were done and we were ready to get the event underway. Participants started pouring in and the available seats were quickly getting occupied. A bunch of the participants were completely unaware of what happened to Zeal Firefox Club and what the new *Mozilla Campus Clubs* deal was. To clear out the dilemma, we gave a small introduction. Our new Teach Lead, Ms. Snehal Samak, was up to the task and she explained the transition from Firefox Student Ambassadors to Mozilla Campus Clubs. After the introduction of the club, it was time to hit the new leads with the spotlight. Everyone introduced themselves and what are their roles and responsibilities are in the club.
Here’s a quick overview of all our leads:

* Club Captain : [Dhanesh B. Sabane](https://mozillians.org/en-US/u/dhanesh95/)
* Club co-Captain : [Shubham Yadav](https://mozillians.org/en-US/u/yadavs4shubham/)
* Technology Lead: Yash Kulkarni
* Teach Lead: Snehal Samak
* Protect Lead: [Shubham Priyadarshi](https://mozillians.org/en-US/u/shubham.priyadarshi/)


## WORKING WITH VERSION CONTROL SYSTEMS – GIT

Moving on we began with the first activity of the day, getting familiar with **git – the stupid content tracker.**

{{< x user="MozClubZeal" id="835380523191128064" >}}

I started with the introduction to the basic concepts of version control systems and explained how we can use them to make our life easier and our work productive. As this was a “hackathon” of sorts, I immediately switched to the hands-on part and covered the following git commands:

```
git init
git add
git commit
git push
git pull
git fetch
git diff
git show
```

People tried their hands at pushing and pulling from their remote GitHub directory and they loved it!

{{< x user="MozClubZeal" id="835396316012371968" >}}


## ONLINE PETITIONS – A WAY TO FIGHT FOR AN OPEN WEB

After a quick lunch break, all the participants were refreshed for the upcoming sessions. Our new lead, Shubham and his compatriot, Prinkle, started with their session. They explained what online petitions are and how they can be instrumental in **#ProtectingtheWeb**. They demonstrated a successful online petition and asked people to sign the ongoing petition against **#InternetShutdowns** in India.

{{< x user="MozClubZeal" id="835407688167194624" >}}
{{< x user="MozClubZeal" id="835408657743507456" >}}


## INTRODUCTION TO WEBVR (A-FRAME)

It was now time to get back to some coding. This was my session again and I displayed a [presentation](https://dhanesh95.gitlab.io/slides/webvr-aframe) about Virtual Reality apps and how everyone can get started with only a browser and a text editor. ([Thanks to Ram Gurumukhi](https://github.com/gurumukhi/aframe-presentation-explorer))

{{< x user="MozClubZeal" id="835421773143367680" >}}

All the participants fired up their text editors and coded their way to their first Hello World program using A-Frame.

{{< x user="MozClubZeal" id="835422171124105217" >}}

A few participants went out of the way and designed some more complex apps with rotating shapes and a few more effects. The quick output that they got by just a page refresh on their browser made this task even more interesting.


## GAME DEVELOPMENT IN PYTHON

The clock was ticking fast and we had already reached the last session of the day. Shubham who had a solid experience in developing basic apps using the [Arcade library](https://pythonhosted.org/arcade/) was the speaker for this session. Our pre-event research concluded that a majority of the participants were completely new to Python. Hence, Shubham first started with some basic concepts of Python and eased the participants into it. After completing the introductory session, he moved on to explaining the arcade library and helped the participants develop a basic game.

{{< x user="MozClubZeal" id="835456345830801409" >}}

By the end of the day, the total coding time was **5 hours**! We asked the participants to give us some feedback on the day and all of them loved it!

This was my first event as a Club Captain and I was ecstatic for the way the event was conducted. I would like to thank all my team members and also all the faculty members of Computer Department, ZES’s ZCOER for their continued support.
