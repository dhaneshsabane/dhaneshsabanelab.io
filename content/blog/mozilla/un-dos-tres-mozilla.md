---
title: Un. Dos.. Tres… Mozilla!
date: 2015-08-20
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
categories: [Community]
author: Dhanesh Sabane
---

## Event details:

- Name : Un. Dos.. Tres... Mozilla!
- Agenda :
    - Introduction to FSA Guide and Firefox Friends
- Day and Date : Saturday, 25th July 2015
- Venue : Computer Department, ZES's Zeal College of Engineering and Research, Narhe, Pune
- Speakers :
    - Nishigandha Yadav
    - Dhanesh Sabane


After the huge success of the inauguration event, the club began with the planning of different activities to be an active contributor of the community. The club had managed to register a total of 65 (and counting) students for the FSA program and the club. Like they say, with great power comes great responsibility, this group of club members was our 'power' and our 'responsibility' was to prepare them to be active contributors and volunteers of the Open Web.

We first decided to start with the basics. Taking baby steps towards success was our plan as there was no point in going for the complicated stuff at the beginner level. Hence, we came up with 'Un. Dos.. Tres... Mozilla!' , an event which gave the students a basic idea about Mozilla and how they can be highly productive student ambassadors. We covered the basic introduction to Mozilla, then the FSA guide (an important one) and Firefox Friends in the event. Basic introduction and the FSA guide was conducted by our club lead while Firefox Friends was conducted by me.
I enlightened the students on the history of the emergence of Firefox Friends program and how it is the most basic way of contributing and getting recognized. I gave an example about our own Club Lead (Nishigandha Yadav) who was featured in the list of top sharers for Firefox Friends in the month of July. Each and every aspect of Firefox Friends was covered.

After the main event, we discussed the further strategies of the club and asked the students to come up with their own views and ideas.

The students responded very well and were eager for the upcoming events of the club!


![Un-dos-tres-1](/images/mozilla/un-dos-tres-1.jpg)

<br />
![Un-dos-tres-2](/images/mozilla/un-dos-tres-2.jpg)

Love Mozilla! <3
