---
title: De-Googlification - Part 2
date: 2016-12-07
tags:
- Open Source
- De-Googlification
author: Dhanesh Sabane
description: Second part of the story of my attempts to remove every trace of Google from my digital life.
categories: [Privacy]
series: De-Googlification
---
> De-Googlification - To detach yourself from "Google's grip" on your Web life.

---
{{< center >}}
<h3>My quest leads me to Android and Google Plus. This one should be easy too.</h3>
{{< /center >}}

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/f/fc/Android_logo_%282015-2019%29.svg" height=50% width=50% alt="Android logo (2014).svg" attr="Google LLC, Public domain, via Wikimedia Commons" attrlink="https://commons.wikimedia.org/w/index.php?curid=39927898" >}}

After getting off to an easy start, my next goal is to replace the Android ROM on my Moto E2. My stock Android ROM is Lolliop(5.1) which comes with a huge bundle of Google Services. Everything on the device is so well knit together that you instantly get comfortable with it and never want to let go. Instant access to a plethora of apps through the Play Store, periodically receiving weather updates, keeping me updated with the latest happenings in the technology world and real-time updates of my favorite football team. The setup was perfect!

However, although all of this enjoyable and convenient, I still need to get rid of it to protect my privacy which brings me to [CyanogenMod](https://www.cyanogenmod.org/). I downloaded the stable release and followed the installation process from the CyanogenMod wiki. The installation didn’t take long and was fairly simple. Fortunately, as I’m using [Fedora](https://getfedora.org/) on my laptop, the tools required to flash the ROM – adb and fastboot – were easily available through the official repository. CyanogenMod OS is an aftermarket OS which provides a host of customization options and other features which are not provided by official Android based firmwares of vendors. Moreover, CyanogenMod is free of Google services[1] and is completely open source. Perfect replacement!

> EDIT (21st June 2020) - CyanogenMod is no longer active. [Lineage OS](https://lineageos.org) is it's successor.

But wait a minute…. how do I install apps? With CyanogenMod, Google’s apps are gone which means Google Play Store is gone. Well, for geeks like me, there are alternatives to Google Play Store too. Enter [F-Droid](https://f-droid.org/) and [Aptoide](http://www.aptoide.com/). Both of these stores are open source but cater to different needs. F-Droid only provides Free and Open Source Android apps which means I can get open source apps from there. For the proprietary apps like Slack, Twitter and mobile shopping, [Aptoide](http://www.aptoide.com/) serves me well.

> EDIT (21st June 2020) - I've come back to this post after a long time. I've been using [Aurora Store](https://auroraoss.com/) in place of Aptoide for the past couple of years now. Much better replacement to Google Play Store.

Unfortunately, there is a small drawback to this strategy.  Many proprietary apps including the ones I mentioned above rely on Google Services Framework.  This means that some apps will work while others won’t.  For example, games like Clash of Clans and secure messengers like [Signal](https://signal.org/) won’t work at all.

> EDIT (13th March 2017) - Today I discovered Signal doesn't need Google Services Framework to run. So now I can use Signal on my CyanogenMod device. God bless Open whisper Systems.

But apps like Slack and a few others will work with a slight compromise on usability (manual checking of updates, periodically, is required as notifications are not provided).

As I [switched to DuckDuckGo](/blog/degooglification-part-1.html), I was hoping to find an alternative to Google Search bar on my device and god bless DuckDuckGo for [providing one](https://duck.co/help/mobile/android).

The only service I now miss is contact sync.  I’ve restored my contacts from the local backup I performed before installing CyanogenMod but I need to find an alternative soon.  Using [ownCloud](https://owncloud.org/) to setup a private cloud is definitely not an option for me as I don’t have the necessary resources.

Finally, with mixed feelings I’ve managed to completely remove Google from my mobile.  Meanwhile, I also discontinued Google Plus which wasn’t much of a concern.

Two more items to cross off of my list!

| Service       | Status      |
|---------------|-------------|
| Google Search | Replaced    |
| Android       | Replaced    |
| Google Plus   | Replaced    |
| Gmail         | In Progress |
| Google Docs   | Still here  |
| Google Drive  | In Progress |
| Google Maps   | Still here  |
| YouTube       | Still here  |

3 down. 5 to go.

[1] Although CyanogenMod is free of Google Services, one can always flash an additional file to install them. See [Google Apps – CyanogenMod](https://wiki.cyanogenmod.org/w/Google_Apps).
